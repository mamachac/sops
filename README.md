# sops



## Install SOPS
```
wget https://github.com/mozilla/sops/releases/download/v3.7.3/sops_3.7.3_amd64.deb
dpkg -i sops_3.7.3_amd64.deb
```


## Generate PGP
```
gpg --gen-key
```

```
pbarczi@zero:~$ gpg --gen-key
gpg (GnuPG) 2.2.27; Copyright (C) 2021 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Note: Use "gpg --full-generate-key" for a full featured key generation dialog.

GnuPG needs to construct a user ID to identify your key.

Real name: pbarczi
Email address: pety.barczi@gmail.com
You selected this USER-ID:
    "pbarczi <pety.barczi@gmail.com>"

Change (N)ame, (E)mail, or (O)kay/(Q)uit? O
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: /home/pbarczi/.gnupg/trustdb.gpg: trustdb created
gpg: key C1E4A538749F387B marked as ultimately trusted
gpg: directory '/home/pbarczi/.gnupg/openpgp-revocs.d' created
gpg: revocation certificate stored as '/home/pbarczi/.gnupg/openpgp-revocs.d/0237CAFF09A3202F0116A6D7C1E4A538749F387B.rev'
public and secret key created and signed.

pub   rsa3072 2022-09-14 [SC] [expires: 2024-09-13]
      0237CAFF09A3202F0116A6D7C1E4A538749F387B
uid                      pbarczi <pety.barczi@gmail.com>
sub   rsa3072 2022-09-14 [E] [expires: 2024-09-13]
```

## Import pub keys
```
gpg --import pgp/sops_functional_tests_key.asc
```

## List keys
```
gpg --list-secret-keys
gpg --list-keys
gpg --import keys/*
```
E.g.:
```
onmetal@az3-zero:~$ gpg --import sops/osc-team-key.priv
gpg: key 5CCB5ACD3AFFBE2D: public key "osc-team (OSC Team)" imported
gpg: key 5CCB5ACD3AFFBE2D: secret key imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg:       secret keys read: 1
gpg:   secret keys imported: 1
```

Verify:
```
onmetal@az3-zero:~$ gpg --list-secret-keys
/home/onmetal/.gnupg/pubring.kbx
--------------------------------
sec   rsa4096 2022-09-13 [SCEA]
      115316093BDB13B25EB9B7DD5CCB5ACD3AFFBE2D
uid           [ unknown] osc-team (OSC Team)
ssb   rsa4096 2022-09-13 [SEA]

onmetal@az3-zero:~$
```

## Delete pub key from keyring
```
gpg --delete-keys 3CA7F1FB50A6904FDF4756954081F0CB0E0C098F
```

## Encrypt file
```
sops -i -e file
```
NOTE: in place

##
```
GPG_TTY=$(tty)
export GPG_TTY
```

## Decrypt file
```
sops -d -i file.yaml
```


>More: 
- https://www.youtube.com/watch?v=YTEVyLXFiq0
- https://poweruser.blog/how-to-encrypt-secrets-in-config-files-1dbb794f7352
